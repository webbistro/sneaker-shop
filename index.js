const express = require('express')
const path = require('path')
const mongoose = require('mongoose')
const Handlebars = require('handlebars')
const exphbs = require('express-handlebars')
const {allowInsecurePrototypeAccess} = require('@handlebars/allow-prototype-access')
const routes = require('./routes/router')
const User = require('./models/user')

const app = express()
const hbs = exphbs.create({
    defaultLayout: 'main',
    extname: 'hbs',
    handlebars: allowInsecurePrototypeAccess(Handlebars)
})

app.engine('hbs', hbs.engine)
app.set('view engine', 'hbs')
app.set('views', 'views')

app.use(async (req, res, next) => {
    try {
        const user = await User.findById('5ea01e0f2f87001348c329dc')
        req.user = user
        next()
    } catch (e) {
        console.log(e)
    }
})

hbs.handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
    switch (operator) {
        case '===':
            return (v1 === v2) ? options.fn(this) : options.inverse(this);
        case '!==':
            return (v1 !== v2) ? options.fn(this) : options.inverse(this);
        case '<':
            return (v1 < v2) ? options.fn(this) : options.inverse(this);
        case '<=':
            return (v1 <= v2) ? options.fn(this) : options.inverse(this);
        case '>':
            return (v1 > v2) ? options.fn(this) : options.inverse(this);
        case '>=':
            return (v1 >= v2) ? options.fn(this) : options.inverse(this);
        case '&&':
            return (v1 && v2) ? options.fn(this) : options.inverse(this);
        case '||':
            return (v1 || v2) ? options.fn(this) : options.inverse(this);
        default:
            return options.inverse(this);
    }
});

app.use(express.static(path.join(__dirname, 'public')))
app.use(express.urlencoded({extended: true}))
app.use(routes)

const PORT = process.env.PORT || 3000

async function start() {
    try {
        const url = `mongodb+srv://artem:kULnyOexysb1ASHy@cluster0-b5dya.mongodb.net/shop`
        await mongoose.connect(url, { 
            useNewUrlParser: true , 
            useUnifiedTopology: true,
            useFindAndModify: false
        })

        const candidate = await User.findOne()
        if(!candidate) {
            const user = new User({
                email: 'test@gmail.com',
                name: 'Test',
                cart: {items: []}
            })

            await user.save()
        }

        app.listen(PORT, () => {
            console.log(`Server is running on port: ${PORT}`);
        })

        Handlebars.registerHelper('ifTr', function (v1, v2, options) {
            return (v1 === v2) ? options.fn(this) : options.inverse(this);    
        });
    } catch(e) {
        console.log(e)
    }
}

start()
