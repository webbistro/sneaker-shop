const {Router} = require('express')
const Shoes = require('../models/shoes')
const router = Router()

router.get('/', (req, res) => {
    res.render('index', {
        title: 'Home page',
        isHome: true
    })
})
router.get('/men', async (req, res) => {
    const shoes = await Shoes.find().populate('userId', 'email name')
    const menShoes = shoes.filter((f) => f.page === 'men')
    res.render('men', {
        title: 'Men page',
        isMen: true,
        menShoes
    })
})
router.get('/women', async (req, res) => {
    const shoes = await Shoes.find().populate('userId', 'email name')
    const womenShoes = shoes.filter((f) => f.page === 'women')

    res.render('women', {
        title: 'Women page',
        isWomen: true,
        womenShoes
    })
})
router.get('/account', (req, res) => {
    res.render('account', {
        title: 'My Account'
    })
})
router.get('/add', (req, res) => {
    res.render('add', {
        title: 'Add Shoes'
    })
})
router.get('/all/:id/edit', async (req, res) => {
    if(!req.query.allow) {
        return res.redirect('/')
    }

    const shoes = await Shoes.findById(req.params.id)
    res.render('shoes-edit', {
        title: shoes.title,
        shoes
    })
})
router.post('/shoes/edit', async (req, res) => {
    const {id} = req.body
    delete req.body.id
    await Shoes.findByIdAndUpdate(id, req.body)
    
    if(req.body.page === 'men') {
        res.redirect('/men')
    }

    if(req.body.page === 'women') {
        res.redirect('/women')
    }
})
router.post('/shoes/remove', async (req, res) => {
    try {
        await Shoes.deleteOne({_id: req.body.id})
        
        if(req.body.page === 'men') {
            res.redirect('/men')
        }
    
        if(req.body.page === 'women') {
            res.redirect('/women')
        }
        
    } catch(e) {
        console.log(e)
    }
})
router.get('/all/:id', async (req, res) => {
    const shoes = await Shoes.findById(req.params.id)
    res.render('shoes', {
        title: shoes.title,
        shoes
    })
})
router.post('/add', async (req, res) => {
    const shoes = new Shoes({
        title: req.body.title,
        price: req.body.price,
        page: req.body.page,
        img: req.body.img,
        userId: req.user
    })
    try {
        await shoes.save()

        if(req.body.page === 'men') {
            res.redirect('/men')
        }
    
        if(req.body.page === 'women') {
            res.redirect('/women')
        }
    } catch(e) {
        console.log(e)
    }
})
router.post('/card/add', async (req, res) => {
    const shoes = await Shoes.findById(req.body.id)
    await req.user.addToCart(shoes)
    res.redirect('/card')
})
router.delete('/card/remove/:id', async (req, res) => {
    const card = await Card.remove(req.params.id)
    res.status(200).json(card)
})
router.get('/card', async (req, res) => {
    const user = await req.user
        .populate('cart.items.courseId')
        .execPopulate()

    res.render('card', {
        title: 'Card',
        shoes: card.shoes,
        price: card.price
    })
})

module.exports = router