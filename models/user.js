const {Schema, model} = require('mongoose')

const userSchema = new Schema({
    email: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    cart: {
        items: [
            {
                count: {
                    type: Number,
                    require: true,
                    default: 1
                },
                shoesId: {
                    type: Schema.Types.ObjectId,
                    ref: 'Shoes',
                    require: true,
                }
            }
        ]
    }
})

userSchema.methods.addToCart = function(shoes) {
    const items = [...this.cart.items]
    const idx = items.findIndex(s => {
        return s.shoesId.toString() === shoes._id.toString()
    })

    if(idx >= 0) {
        items[idx].count = items[idx].count + 1
    } else {
        items.push({
            shoesId: shoes._id,
            count: 1
        })
    }

    // const newCart = { items: clonedItems }
    // this.cart = newCart

    this.cart = {items}
    return this.save()
}

module.exports = model('User', userSchema)