document.addEventListener('DOMContentLoaded', function() {
    let elems = document.querySelectorAll('select');
    let instances = M.FormSelect.init(elems);

    const toCurrency = price => {
        return new Intl.NumberFormat('en-EN', {
            currency: 'USD',
            style: 'currency'
        }).format(price)
    }

    document.querySelectorAll('.price').forEach(item => {
        item.textContent = toCurrency(item.textContent)
    })

    const $card = document.querySelector('#card')
    if($card) {
        $card.addEventListener('click', event => {
            if(event.target.classList.contains('js-remove')) {
                const id = event.target.dataset.id
                
                fetch('/card/remove/' + id, {
                    method: 'delete'
                }).then(res => res.json())
                .then(card => {
                        if(card.shoes.length) {
                            const html = card.shoes.map(s => {
                                return `
                                    <tr>
                                        <td>${s.title}</td>
                                        <td>${s.count}</td>
                                        <td>
                                            <button class="btn btn-small js-remove" data-id="${s.id}">Delete</button>
                                        </td>
                                    </tr>
                                `
                            }).join('')
                            $card.querySelector('tbody').innerHTML = html
                            $card.querySelector('.price').textContent = toCurrency(card.price)
                        } else {
                            $card.innerHTML = '<p>Cart is empty</p>'
                        }
                })
            }
        })
    }
});